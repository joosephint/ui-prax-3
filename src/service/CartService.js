class CartService {
    static appendToCart(id, amount, store) {
        if (amount < 0) {
            return;
        }

        const cart = this.getCart();
        let index = cart.findIndex(p => p.productId === id && p.storeId === store);
        let purchase = {productId: id, storeId: store, amount: 0};
        if (index > -1) {
            purchase = cart[index];
        }
        purchase.amount += amount;
        if (index > -1) {
            cart.splice(index, 1);
        }
        cart.push(purchase);

        this.saveCart(cart);
    };

    static setToCart(id, amount, store) {
        const cart = this.getCart();
        let index = cart.findIndex(p => p.productId === id && p.storeId === store);

        if (amount <= 0) {
            cart.splice(index, 1);
        } else {
            let purchase = {productId: id, storeId: store, amount: 0};
            if (index > -1) {
                purchase = cart[index];
            }
            purchase.amount = amount;
            if (index > -1) {
                cart.splice(index, 1, purchase);
            } else {
                cart.push(purchase);
            }
        }

        this.saveCart(cart);
    }

    static getCart() {
        return JSON.parse(localStorage.getItem('cart') || '[]');
    }

    static saveCart(c) {
        localStorage.setItem('cart', JSON.stringify(c));
    }
}

export default CartService;
