class Service {
    static getProducts() {
        return fetch(`${process.env.PUBLIC_URL}/data/products.json`, {
            method: 'get'
        }).then(function (response) {
            return response.json();
        });
    }

    static getCategories() {
        return fetch(`${process.env.PUBLIC_URL}/data/categories.json`, {
            method: 'get'
        }).then(function (response) {
                return response.json();
        });
    }
}

export default Service;
