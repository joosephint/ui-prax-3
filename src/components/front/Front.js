import React, { Component } from 'react';
import './Front.less'
import Service from "../../service/Service";

class Front extends Component {

    state = {
        categories:  []
    };

    constructor(props) {
        super(props);
        Service.getCategories().then(c => {
            const categories = c.map(c =>
                <a key={`category-${c.id}`} className="category-item" href={`${process.env.PUBLIC_URL}/#/products/category/${c.id}`}>{c.name}</a>
            );
            this.setState({categories})
        });
    }

    render() {
        return (
            <div>
                <div className="jumbotron promo-banner promo-banner-food">

                </div>
                <div className="promo-text">
                    Big promo text lorem dorem ipsum
                </div>
                <div className="category">
                    {this.state.categories}
                </div>
            </div>
        );
    }
}

export default Front;
