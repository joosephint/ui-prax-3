import React, {Component} from 'react';
import './Cart.less'
import CartService from "../../service/CartService";
import Service from "../../service/Service";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {toast} from "react-toastify";

class Cart extends Component {

    state = {
        cart: CartService.getCart(),
        cartItems: [],
        canPay: false,
        totalPrice: 0
    };

    products;


    constructor(props) {
        super(props);
        Service.getProducts().then(c => {
            this.products = c;
            this.setCartItems();
        });
    }

    render() {
        let purchase = '';
        if (this.state.totalPrice) {
        purchase =
            <div>
                <input className="input input--address" type="text" placeholder="Aadress" onChange={t => this.updateAddress(t.target.value)}/>
                <div className="title title--margin-top">
                    Ostuviis
                </div>
                <div className="payment">
                    <button className="payment__item" disabled={!this.state.canPay} onClick={() => this.purchase()}>
                        Swedbank
                    </button>
                    <button className="payment__item" disabled={!this.state.canPay} onClick={() => this.purchase()}>
                        SEB
                    </button>
                    <button className="payment__item" disabled={!this.state.canPay} onClick={() => this.purchase()}>
                        PayPal
                    </button>
                </div>
            </div>;
        }

        return (
            <div>
                <h1 className="title">
                    Ostukorv
                </h1>
                <div className="cart">
                    {this.state.cartItems}
                    <div className={"cart__price" + (this.state.totalPrice ? '' : ' cart__price--left')}>
                        <a className="cart__price__text cart__price__text--sub">{this.state.totalPrice ? 'Tarne hind: 3€': ''}</a>
                        <a className="cart__price__text">
                            {this.state.totalPrice ? `Kogu hind: ${(this.state.totalPrice + 3).toFixed(2)}€` : 'Ostukorv on tühi'}
                        </a>
                    </div>
                </div>
                {purchase}
            </div>
        );
    }

    setCartItems() {
        const state = Object.assign({}, this.state);
        state.totalPrice = 0;
        state.cartItems = this.state.cart.map(c => {
            let productId = +c.productId;
            let store = c.storeId;
            let product = this.products.find(p => p.id === productId);
            let productStore = product.stores.find(s => s.id === store);
            let productPrice = (parseFloat(productStore.price) * c.amount);
            state.totalPrice += productPrice;

            return <div className="cart__item" key={`cart-${c.productId}-${c.storeId}`}>
                <div className="cart__item__amount">
                    <div className="amount-change" onClick={() => this.addToItem(c, 1)}>+</div>
                    {c.amount}
                    <div className="amount-change" onClick={() => this.addToItem(c, -1)}>-</div>
                </div>
                <div className="product__item__text__main">
                    <div>{product.name}</div>
                    <div>{productPrice.toFixed(2)}€</div>
                </div>
                <div className="cart__logo">
                    {productStore.name}
                </div>
                <FontAwesomeIcon className="cursor-pointer" icon={faTrash}
                                 onClick={() => this.removeFromCart(c)}/>
            </div>
        });
        this.setState(state);
    }

    removeFromCart(cartItem) {
        console.log(cartItem);
        CartService.setToCart(cartItem.productId, -1, cartItem.storeId);
        let product = this.products.find(p => p.id === +cartItem.productId);
        const productStore = product.stores.find(s => s.id === +cartItem.storeId);
        const state = Object.assign({}, this.state);
        state.cart = CartService.getCart();
        state.totalPrice -= parseFloat(productStore.price) * cartItem.amount;
        this.setState(state, () => this.setCartItems());
    }

    updateAddress(text) {
        const state = Object.assign({}, this.state);
        state.canPay = !!text.length;
        this.setState(state);
    }

    addToItem(cartItem, amount) {
        const state = Object.assign({}, this.state);
        CartService.setToCart(cartItem.productId, cartItem.amount + amount, cartItem.storeId);
        state.cart = CartService.getCart();
        this.setState(state, () => this.setCartItems());
    }

    purchase() {
        CartService.saveCart([]);
        toast('Item is on your way!');
        setTimeout(() => window.location.href = '#/', 300);
    }
}

export default Cart;
