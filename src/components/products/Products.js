import React, {Component} from 'react';
import './Products.less'
import Service from "../../service/Service";

class Products extends Component {
    state = {
        products: [],
        categories: [],
        selectedCategory: null,
        searchContent: null,
    };

    productData;

    constructor(props) {
        super(props);
        Service.getCategories().then(categoryList => {
            const categories = [
                <a key="dropdown-0" className="dropdown-item" onClick={() => this.changeCategory()}>---</a>
            ];
            categories.push(categoryList.map((c, i) =>
                <a key={`dropdown-${i}`} className="dropdown-item"
                   onClick={() => this.changeCategory(c.name, c.id)}>{c.name}</a>
            ));
            this.setState({categories})
            Service.getProducts().then(c => {
                let category = {};
                let categoryId = props.match.params.category;
                if (categoryId) {
                    category = categoryList.find(cat => cat.id === +categoryId);
                }
                this.productData = c;
                this.updateProducts(category.name);
            });
        });
    }

    updateProducts(category, searchContent) {
        if (searchContent) {
            searchContent = searchContent.toLowerCase();
        }
        let productData = this.productData
            .filter(c => !category || c.category === category)
            .filter(c => !searchContent || c.name.toLocaleLowerCase().includes(searchContent)
                || c.description.toLocaleLowerCase().includes(searchContent));

        const products = productData.map(c =>
            <div key={`product-${c.id}`} className="product__item"
                 onClick={() => window.location = '#/products/' + c.id}>
                <div className="product__item__picture">
                    <img src={c.imageUrl} alt={c.name}/>
                    <div className="product__item__price">
                        {`${this.lowestPrice(c.stores)}€`}
                    </div>
                </div>
                <div className="product__item__text">
                    <div className="product__item__text__main">
                        {c.name}
                    </div>
                    <div className="product__item__text__main--sub">
                        {c.description}
                    </div>
                </div>
                <div className="product__item__price--small">
                    {`${this.lowestPrice(c.stores)}€`}
                </div>
            </div>
        );
        this.setState({
            products: products,
            selectedCategory: category,
            searchContent: searchContent,
        });
    }

    render() {
        return (
            <div>
                <h1 className="title">
                    Tooted
                </h1>
                <div className="filters">
                    <button type="button" className="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false">
                        {this.state.selectedCategory || 'Filtreeri tooteid'}
                    </button>
                    <div className="dropdown-menu">
                        {this.state.categories}
                    </div>
                    <input className="input search" type="text" placeholder="Otsi..."
                           onChange={t => this.updateText(t)}/>
                </div>
                <div className="product">
                    {this.state.products}
                </div>
            </div>
        );
    }

    lowestPrice(stores) {
        stores.sort((a, b) => a.price.localeCompare(b.price));
        return stores[0].price;
    }

    changeCategory(name, id) {

        let location = '#/products';
        if (name && id) {
            location += '/category/' + id;
        }
        document.location.href=`${process.env.PUBLIC_URL}/${location}`;
        this.updateProducts(name, this.state.searchContent);
    }

    updateText(text) {
        this.updateProducts(this.state.selectedCategory, text.target.value);
    }
}

export default Products;
