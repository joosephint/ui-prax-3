import React, { Component } from 'react';
import './Contact.less'

class Contact extends Component {
    render() {
        return (
            <div className="contact">
                <div className="contact__map">

                </div>
                <div className="contact__info">
                    <div className="contact__info__item">
                        ValiToit OÜ
                    </div>
                    <div className="contact__info__item">
                        Tel: 5030343
                    </div>
                    <div className="contact__info__item">
                        e-mail: info@valitoit.ee
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;
