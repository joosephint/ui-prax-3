import React, { Component } from 'react';
import './Header.less'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

import logo from '../../assets/valitoit.png'

class Header extends Component {
    render() {
        return (
            <header className="header">
                <a className="header__logo" href={`${process.env.PUBLIC_URL}/#/`}>
                    <img alt="ValiToit" src={logo}/>
                </a>
                <div className="header__nav">
                    <a className="header__nav__item" href={`${process.env.PUBLIC_URL}/#/products`}>Tooted</a>
                    <a className="header__nav__item" href={`${process.env.PUBLIC_URL}/#/about`}>Firmast</a>
                    <a className="header__nav__item" href={`${process.env.PUBLIC_URL}/#/contact`}>Kontakt</a>
                    <div className="header__nav__cart" onClick={() => window.location.href=`${process.env.PUBLIC_URL}/#/cart`}>
                        <FontAwesomeIcon icon={faShoppingCart}/>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;
