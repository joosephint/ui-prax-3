import React, {Component} from 'react';
import './Product.less'
import Service from "../../service/Service";
import CartService from "../../service/CartService";
import {toast} from "react-toastify";

class Product extends Component {

    state = {
        product: {},
        amount: 0,
        addToCartEnabled: false,
    };

    selectedStoreId;

    commaFilter = new RegExp(/,|\./);

    constructor(props) {
        super(props);
        Service.getProducts().then(c => {
            this.setState({product: c.filter(p => p.id === +this.props.match.params.id)[0]});
        });
    }

    render() {
        let stores = [];
        if (this.state.product && this.state.product.stores) {
            if (!this.selectedStoreId) {
                this.selectedStoreId = this.state.product.stores[0].id;
            }
            stores = this.state.product.stores.map(s =>
                <div key={`store-${s.id}`}
                     className={'store' + (this.selectedStoreId === s.id ? ' store--selected' : '')}
                     onClick={() => this.setSelectedStore(s.id)}>
                    <div className="store__image">
                        {s.name}
                    </div>
                    <a className="store__price">{`${s.price}€`}</a>
                </div>
            );
        }
        return (
            <div>
                <h1 className="title">
                    {this.state.product.name}
                </h1>
                <div className="productitem">
                    <div className="productitem__image">
                        <img src={this.state.product.imageUrl} alt="Food"/>
                    </div>
                    <div className="description">
                        {this.state.product.description}
                    </div>
                    <div className="productitem__stores">
                        {stores}
                    </div>
                    <div className="add-cart">
                        <div className="amount">
                            <input className="input" id="amount" type="number" placeholder="Kogus"
                                   onChange={t => this.changeAmount(t)}/>
                        </div>
                        <button className="button add-cart" onClick={() => this.addToCart()}
                                disabled={!this.state.addToCartEnabled}>
                            {this.state.addToCartEnabled ? 'Lisa korvi' : 'Sisesta kogus'}
                        </button>
                    </div>
                </div>
            </div>
        );
    }

    setSelectedStore(id) {
        this.selectedStoreId = id;
        this.setState(this.state);
    }

    changeAmount(a) {
        const number = a.target.value;
        const state = Object.assign({}, this.state);
        if (isNaN(+number) || +number <= 0 || +number > 10 || this.commaFilter.test(number)) {
            state.addToCartEnabled = false;
            this.setState(state);
        } else {
            state.addToCartEnabled = true;
            state.amount = +number;
            this.setState(state);
        }
    }

    addToCart() {
        CartService.appendToCart(this.state.product.id,
            this.state.amount, this.selectedStoreId);
        const state = Object.assign({}, this.state);
        state.amount = 0;
        state.addToCartEnabled = false;
        this.setState(state);
        document.getElementById('amount').value = '';
        toast('Added to cart');
    }
}

export default Product;
