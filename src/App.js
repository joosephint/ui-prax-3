import React, {Component} from 'react';
import Header from "./components/header/Header";
import {BrowserRouter, HashRouter, Route, Switch} from "react-router-dom";
import Front from "./components/front/Front";
import About from "./components/about/About";
import Contact from "./components/contact/Contact";
import Products from "./components/products/Products";
import Product from "./components/product/Product";
import Cart from "./components/cart/Cart";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                <ToastContainer className='toast-container'/>
                <HashRouter basename="/~guhint/kasutajaliidesed/prax3">
                    <Switch>
                        <Route exact path="/" component={Front}/>
                        <Route exact path="/about" component={About}/>
                        <Route exact path="/contact" component={Contact}/>
                        <Route exact path="/products/:id" component={Product}/>
                        <Route exact path="/products/category/:category" component={Products}/>
                        <Route path="/products" component={Products}/>
                        <Route exact path="/cart" component={Cart}/>
                    </Switch>
                </HashRouter>
            </div>
        );
    }
}

export default App;
